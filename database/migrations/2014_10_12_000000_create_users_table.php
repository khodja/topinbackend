<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('phone');
            $table->date('birthday')->nullable();
            $table->string('email')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('is_business')->default(false);
            $table->boolean('is_paid')->default(false);
            $table->string('sex')->default('male');
            $table->string('country')->nullable();
            $table->string('address')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->string('website')->nullable();
            $table->string('description')->nullable();
            $table->string('work_time')->nullable();
            $table->string('inn')->nullable();
            $table->string('notification_token')->nullable();
            $table->unsignedInteger('limit')->default(30);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
