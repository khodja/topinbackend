<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adverts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreignId('region_id');
            $table->foreignId('category_id');
            $table->string('name');
            $table->string('description');
            $table->string('address_info')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('promoted_count')->default(0);
            $table->unsignedInteger('price');
            $table->string('status')->default('MODERATION');  // DISABLED MODERATION ACTIVE
            $table->string('currency')->default('UZS');
            $table->string('lat')->nullable();
            $table->string('long')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adverts');
    }
}
