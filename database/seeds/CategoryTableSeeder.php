<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::create([
            'name_ru'=>'Електроника',
            'name_uz'=>'Elektronika',
            'name_kr'=>'Elektronika',
            'description_ru'=>'Електроника Електроника Електроника',
            'description_uz'=>'Elektronika Elektronika Elektronika',
            'description_kr'=>'Електроника Електроника Електроника'
        ]);
    }
}
