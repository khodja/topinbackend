<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass guarded.
     *
     * @var array
     */
    protected $guarded = [];
    protected $appends = ['registered','banner','image', 'advert_count'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function hasRole($role)
    {
        return $this->roles()->where('type', $role)->first();
    }

    public function adverts()
    {
        return $this->hasMany('App\Advert');
    }

    public function balance()
    {
        return $this->hasOne('App\Balance');
    }

    public function balanceHistory()
    {
        return $this->hasOne('App\BalanceHistory');
    }

    public function favorites()
    {
        return $this->hasMany('App\Favorite');
    }

    public function getAdvertCountAttribute()
    {
        return $this->adverts()->count();
    }

    public function getRegisteredAttribute()
    {
        return strlen($this->name) > 0;
    }

    public function getImageAttribute()
    {
        $directory = "uploads/user/".$this->id;
        $images = \File::glob($directory."/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return null;
    }
    public function getBannerAttribute()
    {
        $directory = "uploads/banner/".$this->id;
        $images = \File::glob($directory."/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/no-banner.png');
    }
}
