<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $guarded = [];
    protected $appends = ['image'];

    public function getImageAttribute()
    {
        $directory = "uploads/slider/" . $this->id;
        $images = \File::glob($directory . "/*.jpg");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/category.png');
    }
}
