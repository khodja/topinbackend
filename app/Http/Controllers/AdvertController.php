<?php

namespace App\Http\Controllers;

use App\BalanceHistory;
use App\Promo;
use App\Region;
use App\Type;
use Illuminate\Http\Request;
use App\Advert;
use App\AdvertView;
use App\Category;
use Auth;
use Intervention\Image\ImageManagerStatic as Image;

class AdvertController extends Controller
{
    public function get()
    {
        //where('status', 'MODERATION')->
        $adverts = Advert::with('region')->where("status", 'ACTIVE')->take(24)->get();
        return response()->json(['data' => $adverts, 'statusCode' => 200], 200);
    }

    public function activate($id)
    {
        $advert = Advert::findOrFail($id);
        $advert->update(['status' => 'ACTIVE']);
        return redirect()->back()->with('success', 'Успешно');
    }

    public function disable($id)
    {
        $advert = Advert::findOrFail($id);
        $advert->update(['status' => 'DISABLED']);
        return redirect()->back()->with('success', 'Успешно');
    }


    public function promote($id)
    {
        $user = Auth::user();
        $balance = $user->balance;
        $advert = Advert::where([['user_id', $user->id], ['id', $id]])->firstOrFail();
        $promo = Promo::findOrFail(\request('promo_id') * 1);
        if ($balance->amount * 1 < $promo->price) {
            return response()->json(['statusCode' => 404], 404);
        }
        $is_success = $balance->subtract($promo->price * 1);
        if ($is_success) {
            BalanceHistory::create([
                'user_id' => $user->id,
                'balance_id' => $balance->id,
                'amount' => $promo->price * -1,
            ]);
            $advert->update([
                'promoted_count' => $advert->promoted_count * 1 + $promo->count * 1
            ]);
        }
        return response()->json(['statusCode' => 200], 200);
    }

    public function show(Advert $advert, $id)
    {
        $advert = $advert->with('advert_type', 'user')->find($id);
        $advert_type = $advert->advert_type;
        $getTypeByValues = $advert_type->where('changeable', '!=', 1)->pluck('pivot')->pluck('value');
        $chosen_types = Type::whereIn('id', $getTypeByValues)->get();
        foreach ($advert_type as $key => $type) {
            if ($type->changeable * 1 !== 1) {
                $advert->advert_type[$key]->pivot->value = $chosen_types->where('id',
                    $advert->advert_type[$key]->pivot->value)->first()->name_ru;
            }
        }
        if ($advert->isViewed()) {      // this will test if the user viwed the post or not
            return $advert;
        }

        $advert->increment('views'); // increments count of views
        if ($advert->promoted_count > 0) {
            $advert->decrement('promoted_count');
        }
        AdvertView::createViewLog($advert);
        return $advert;
    }

    private $validation = [
        'name' => 'required',
        'price' => 'required',
        'category_id' => 'required',
        'region_id' => 'required',
        'advert_type' => 'required',
    ];

    public function index()
    {
        $data = Advert::all();
        return view('backend.advert.index', compact('data'));
    }

    public function own()
    {

        $adverts = Advert::where([['user_id', Auth::user()->id], ['status', request('status')]])->with('region',
            'advert_type')->get();
        return response()->json(['data' => $adverts, 'statusCode' => 200], 200);
    }

    public function showOwn($id)
    {
        $advert = Advert::where([['id', $id], ['user_id', Auth::user()->id]])->with('region',
            'advert_type')->firstOrFail();
        $advert_type = $advert->advert_type;
        $getTypeByValues = $advert_type->where('changeable', '!=', 1)->pluck('pivot')->pluck('value');
        $chosen_types = Type::whereIn('id', $getTypeByValues)->get();
        foreach ($advert_type as $key => $type) {
            if ($type->changeable * 1 !== 1) {
                $advert->advert_type[$key]->pivot->value = $chosen_types->where('id',
                    $advert->advert_type[$key]->pivot->value)->first();
            }
        }
        return response()->json(['data' => $advert, 'statusCode' => 200], 200);
    }

    public function create()
    {
        $category_id = request('category_id');
        $category = Category::findOrFail($category_id);
        $regions = Region::all();
        $types = Type::with('children')->where('category_id', $category_id)->get();
        return view('backend.advert.create', compact('regions', 'types', 'category'));
    }

    public function edit($id)
    {
        $data = Advert::with('advert_type')->findOrFail($id);
        $category = Category::findOrFail($data->category_id);
        $regions = Region::all();
        $types = Type::where('category_id', $data->category_id)->get();
        $directory = "uploads/advert/" . $data->id;
        $images = \File::glob($directory . "/*.jpg");
        $chosen_types = $data->advert_type;
        return view('backend.advert.edit', compact('data', 'types', 'category', 'regions', 'chosen_types', 'images'));
    }

    public function add($request)
    {
        $request['type_id'] = is_array($request['type_id']) ? $request['type_id'] : json_decode($request['type_id']);
        $request['advert_type'] = is_array($request['advert_type']) ? $request['advert_type'] : json_decode($request['advert_type']);
        $request['user_id'] = Auth::user()->id;
        $advert = Advert::create($request->except('_token', 'advert_type', 'images', 'type_id', 'phone'));
        if (!is_null($request['type_id']) && !is_null($request['advert_type'])) {
            $type = Type::whereIn('id', $request['type_id'])->get();
            $this->handleAdvertType($request['advert_type'], $request['type_id'], $type, $advert);
        }
//        $advert_type_data = array();
//        foreach ($request['advert_type'] as $key => $input_value) {
//            $get_type = $type->where('id', $request['type_id'][$key])->first();
//            if (!$get_type) {
//                abort(404);
//            }
//            $advert_type_data[$get_type->id] = [
//                'value' => 1 * $input_value
//            ];
//        }
//        $advert->advert_type()->sync($advert_type_data);
        return $advert;
    }

    public function store(Request $request)
    {
        $advert = $this->add($request);
        $this->imageHandle($request, $advert);
        return redirect()->action('AdvertController@index')
            ->with('success', 'Успешно добавлено');
    }

    public function addAdvert(Request $request)
    {
        $advert = $this->add($request);
        $this->imageHandle($request, $advert);
        return response()->json(['statusCode' => 200], 200);
    }

    public function imageHandle($request, $advert)
    {
        if ($request->file('images')) {
            $this->createFolder('advert', $advert->id);
            $this->storeImages($request->file('images'), $advert->id);
        }
    }

    public function createFolder($directory, $id)
    {
        $path = public_path('uploads/' . $directory . '/' . $id);
        $directory_path = public_path('uploads/' . $directory);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return true;
    }

    public function storeImages($images, $id)
    {
        $watermark = Image::make(public_path('/img/logo.png'));
        foreach ($images as $key => $image) {
            $filename = strtotime("now") . $key . '.jpg';
            $path = public_path('uploads/advert/' . $id . '/' . $filename);
            $img = Image::make($image->getRealPath())->encode('jpg', 100);
            $resizePercentage = 80;
            $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100), 2);
            $watermark->resize($watermarkSize, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->insert($watermark, 'bottom-right', -20, 20);
            $img->save($path);
        }
        return 1;
    }

    public function updateAdvert(Request $request, $id)
    {
        $user_id = Auth::user()->id;
        $update_array = [['id', $id]];
        if ($user_id != 1) {
            array_push($update_array, ['user_id', $user_id]);
        }
        $advert = Advert::where($update_array)->firstOrFail();
        if (!is_null($request['type_id']) && !is_null($request['advert_type'])) {
            $types = Type::with('children')->where('category_id', $advert->category_id)->get();
            foreach ($types as $type) {
                $children = $type->children;
                if ($children) {
                    foreach ($children as $child) {
                        $types->push($child);
                    }
                }
                $types = $types->unique();
                $this->handleAdvertType($request['advert_type'], $request['type_id'], $types, $advert);
            }
        }
        $request['status'] = 'MODERATION';
        $status = $advert->update($request->except('_token',
            '_method', 'advert_type', 'type_id', 'images'));
        $this->imageHandle($request, $advert);
        return $status;
    }

    public function updateApi(Request $request, $id)
    {
        $status = $this->updateAdvert($request, $id);
        return response()->json(['statusCode' => $status ? 200 : 204], 200);
    }

    public function update(Request $request, $id)
    {
        $this->updateAdvert($request, $id);
        return redirect()->action('AdvertController@index')->with('success',
            'Успешно изменен!');
    }

    public
    function search()
    {
        $keyword = request('keyword');
        $price_range = request('price_range');
        $region = request('region_id');
        $region = $region * 1;
        $category = request('category_id');
        $sortBy = request('sort_by'); /// created_at \ price
        $orderBy = request('order_by') === 'ASC' ? 'ASC' : 'DESC';
        $type_value = request('type_value');
        $user_id = request('user_id');
        $limit = request('limit');
        if (!$limit) {
            $limit = 20;
        }
        $advert = Advert::where('status', 'ACTIVE')   // DISABLED MODERATION ACTIVE
        ->with('region', 'user');
        if ($region) {
            $advert->where('region_id', $region);
        }
        if ($keyword) {
            $advert = $advert->where('name', 'like', '%' . $keyword . '%');
        }
        if ($user_id) {
            $advert = $advert->where('user_id', $user_id);
        }
        if ($sortBy && $orderBy) {
            $advert = $advert->orderBy($sortBy, $orderBy);
        }
        if ($category) {
            $categories = Category::where('id', $category)->firstOrFail();
            $parent = collect([$categories->id * 1]);
            $subs = $categories->getAllChilds()->pluck('id');
            $cat_ids = $parent->merge($subs);
            $advert = $advert->whereIn('category_id', $cat_ids);
        }
        if ($price_range) {
            $range = explode(',', $price_range);
            $advert = $advert->where('price', '>=', $range[0] * 1)->where('price', '<=', $range[1] * 1);
        }
        if ($type_value) {
            $advert = $advert->has('advert_type');
            $type_value = json_decode($type_value, true);
            // type_value = [id, value] value = [child_id] || [changeable_value,between]
            foreach ($type_value as $values) {
                $advert = $advert->whereHas('advert_type', function ($query) use ($values) {
                    $id = $values[0];
                    $selected = $values[1];
                    $count = count($selected);
                    if (isset($selected[0])) {
                        if ($count === 1) {
                            $search_data = [
                                ['value', '=', $selected[0]],
                                ['type_id', '=', $id]
                            ];

                            $query->where($search_data);
                        } else {
                            if ($count === 2) {
                                $query->where('type_id', $id);
                                $query->whereBetween('value', [$selected[0], $selected[1]]);
                            }
                        }
                    } else {
                        $query->where('value', '-1');
                    }
                });
            }

        }

        $advert = $advert->paginate($limit); //page query pagination
        return $advert;
    }

    //// helpers
    public
    function handleAdvertType(
        $advert_type,
        $type_chosen,
        $types,
        Advert $advert
    ): void
    {
        $advert_type_data = [];
        foreach ($advert_type as $key => $input_value) {
            $get_type = $types->where('id', $type_chosen[$key])->first();
            if (!!$get_type) {
                $advert_type_data[$get_type->id] = [
                    'value' => 1 * $input_value
                ];
            }

        }
        $advert->advert_type()->sync($advert_type_data);
    }

    public function removeImage($id)
    {
        $advert = Advert::findOrFail($id);
        if (Auth::user()->id != 1 && Auth::user()->id != $advert->user_id) {
            abort();
        }
        $file_name = request('file_name');
        $pathToDestroy = public_path('uploads/advert/' . $id . '/' . $file_name);
        \File::delete($pathToDestroy);
        return response()->json(['statusCode' => 200], 200);
    }

    public function switchStatus($id)
    {
        $advert = Advert::where([['id', $id], ['user_id', Auth::user()->id]])->firstOrFail();
        $status = $advert->status;
        if ($status == 'DISABLED') {
            $status = 'MODERATION';
        } else {
            $status = 'DISABLED';
        }
        $advert->update(['status' => $status]);
        return response()->json(['statusCode' => 200], 200);
    }
}
