<?php

namespace App\Http\Controllers;

use App\Advert;
use Illuminate\Http\Request;
use App\Message;
use Auth;

class MessageController extends Controller
{
    public function list()
    {
        $user = Auth::user();
        $messages = Message::where('user_id', $user->id)->orWhere('receiver_id',
            $user->id)->with('advert.user')->orderBy('created_at', 'DESC')->get();
        $messages = $messages->unique('advert_id')->toArray();
        $messages = array_values($messages);
        return response()->json(['data' => $messages, 'statusCode' => 200], 200);
    }

    public function show($id)
    {
        $user = Auth::user();
        $advert = Advert::where('id', $id)->with('user', 'region')->firstOrFail();
        $messages = Message::where([
            ['advert_id',$advert->id],['user_id', '=', $user->id],
        ])->orWhere([
            ['advert_id',$advert->id],['receiver_id', '=', $user->id]
        ])->get();
//        $messages = Message::where('user_id', $user->id)->orWhere('receiver_id',$user->id)->orderBy('created_at', 'DESC')->get();
        return response()->json(['data' => ['messages' => $messages, 'advert' => $advert], 'statusCode' => 200], 200);
    }

    public function send()
    {
        request()->validate([
            'body' => 'required',
            'advert_id' => 'required',
            'receiver_id' => 'required',
        ]);
        $user = Auth::user();
        $advert = Advert::findOrFail(request('advert_id') * 1);
        $receiver_id = request('receiver_id') * 1;
        if ($user->id * 1 === $receiver_id) {
            abort();
        }
        Message::create([
            'user_id' => $user->id,
            'advert_id' => $advert->id,
            'receiver_id' => $receiver_id,
            'body' => request('body')
        ]);
        return response()->json(['statusCode' => 200], 200);
    }
}
