<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class BalanceController extends Controller
{
    public function get()
    {
        $balance = Auth::user()->balance->amount * 1;
        return response()->json(['data' => $balance, 'statusCode' => 200], 200);
    }
}
