<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Region;

class RegionController extends Controller
{
    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_kr' => 'required',
        'lat' => 'required',
        'long' => 'required',
    ];

    public function get()
    {
        $data = Region::all();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function index()
    {
        $data = Region::whereNull('parent_id')->get();
        return view('backend.region.index', compact('data'));
    }

    public function show($id)
    {
        $data = Region::where('parent_id', $id)->with('children', 'parent')->get();
        $parent = Region::findOrFail($id);
        return view('backend.region.index', compact('data', 'parent'));
    }

    public function create()
    {
        $data = Region::all();
        return view('backend.region.create', compact('data'));
    }

    public function edit($id)
    {
        $data = Region::findOrFail($id);
        $categories = Region::where('id', '!=', $id)->get();
        return view('backend.region.edit', compact('data', 'categories'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }

        $category = Region::create($request->except('_token'));
        if ($category->parent_id !== null) {
            return redirect()->action('RegionController@show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->action('RegionController@index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }
        $category = Region::findOrFail($id);
        $category->update($request->except('_token', '_method'));
        if ($category->parent_id !== null) {
            return redirect()->action('RegionController@show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->action('RegionController@index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function toggleMain($id)
    {
        $data = Region::findOrFail($id);
        $data->update([
            'main' => !$data->main
        ]);
        return redirect()->back();
    }
}
