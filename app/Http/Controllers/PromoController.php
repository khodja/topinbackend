<?php

namespace App\Http\Controllers;

use App\Promo;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_kr' => 'required',
        'description_uz' => 'required',
        'description_ru' => 'required',
        'description_kr' => 'required',
        'price' => 'required',
        'count' => 'required',
    ];
    public function get()
    {
        $data = Promo::all();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }
    public function index()
    {
        $data = Promo::all();
        return view('backend.promo.index', compact('data'));
    }

    public function create()
    {
        return view('backend.promo.create');
    }

    public function edit($id)
    {
        $data = Promo::findOrFail($id);
        return view('backend.promo.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);

        Promo::create($request->except('_token'));
        return redirect()->action('PromoController@index')
            ->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $alias)
    {
        $request->validate($this->validation);
        $promo = Promo::where('alias', $alias)->firstOrFail();
        $promo->update($request->except('_token', '_method'));
        return redirect()->action('PromoController@index')
            ->with('success', 'Успешно');
    }
}
