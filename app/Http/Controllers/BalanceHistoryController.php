<?php

namespace App\Http\Controllers;

use App\BalanceHistory;
use App\User;
use Illuminate\Http\Request;
use Auth;
use GuzzleHttp\Client;

class BalanceHistoryController extends Controller
{
    public function get()
    {
        $data = Auth::user()->balanceHistory()->get();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function octo()
    {
        $user = Auth::user();
        $locale = 'ru';
        if ($locale == 'ru') {
            $text = 'Пополнение баланса на topin.uz';
        }
        $price = request('price') * 1;
        $client = new Client();
        $response = $client->post('https://secure.octo.uz/prepare_payment', [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            'json' => [
                "octo_shop_id" => 2470,
                "octo_secret" => "585316b3-06ae-4305-bc57-2601a4cce005",
                "shop_transaction_id" => time() . strval($user->id),
                "auto_capture" => true,
                "test" => false,
                "init_time" => date("Y-m-d H:i:s"),
                "user_data" => [
                    "user_id" => $user->id,
                    "phone" => $user->phone,
                    "email" => $user->email,
                    "amount" => $price,
                ],
                "total_sum" => number_format($price, 2, '.', ''),
                "currency" => "UZS",
                "description" => $text,
                "payment_methods" => [
                    [
                        "method" => "bank_card"
                    ]
                ],
                "return_url" => "https://topin.uz/operations",
                "notify_url" => "https://admin.topin.uz/api/payment",
                "language" => $locale,
                "ttl" => 15
            ]
        ]);
        $data = json_decode($response->getBody()->getContents());
        return response()->json(['data' => $data->octo_pay_url, 'statusCode' => 200], 200);
    }

    public function uzcard()
    {
        $user = Auth::user();
        $locale = 'ru';
        if ($locale == 'ru') {
            $text = 'Пополнение баланса TOPIN';
        }
        $price = request('price') * 1;
        $client = new Client();
        $response = $client->post('https://secure.octo.uz/prepare_payment', [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            'json' => [
                "octo_shop_id" => 2470,
                "octo_secret" => "585316b3-06ae-4305-bc57-2601a4cce005",
                "shop_transaction_id" => time() . strval($user->id),
                "auto_capture" => true,
                "test" => false,
                "init_time" => date("Y-m-d H:i:s"),
                "user_data" => [
                    "user_id" => $user->id,
                    "phone" => $user->phone,
                    "email" => $user->email,
                    "amount" => $price,
                ],
                "total_sum" => $price,
                "currency" => "UZS",
                "description" => $text,
                "payment_methods" => [
                    [
                        "method" => "uzcard"
                    ],
                    [
                        "method" => "humo"
                    ]
                ],
                "return_url" => "http://topin.indev.uz/operations",
                "notify_url" => "http://topin-system.indev.uz/api/payment",
                "language" => $locale,
                "ttl" => 15
            ]
        ]);
        $data = json_decode($response->getBody()->getContents());
        return response()->json(['data' => $data->octo_pay_url, 'statusCode' => 200], 200);
    }

    public function handlePayment(Request $request)
    {
        $status = $request->status;
        $transaction_id = $request->shop_transaction_id;
        $length = strlen($transaction_id);
        $user_id = substr($request->shop_transaction_id, 10, $length) * 1;
        $payment_uuid = $request->octo_payment_UUID;
        $transfer_sum = $request->transfer_sum * 1;
        if ($status == 'waiting_for_capture') {
            $response = [
                'accept_status' => 'capture'
            ];
            return response()->json($response);
        }
        if ($status == 'succeeded') {
            $user = User::find($user_id);
            $balance = $user->balance;
            BalanceHistory::create([
                'user_id' => $user->id,
                'balance_id' => $balance->id,
                'amount' => $transfer_sum,
                // 99.2 = $transfer_sum
                // 100 = x
                // x = $transfer_sum*100/99.2
                // if sum is with tax take sum as 99.2% otherwise take it as 108%
            ]);
            $balance->add($transfer_sum);
            return response('ok', 200);
        }
    }
}
