<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Favorite;
use Illuminate\Http\Request;
use Auth;

class FavoriteController extends Controller
{
    public function get()
    {
        $data = Favorite::where('user_id',Auth::user()->id)->with('advert.region')->get();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function set()
    {
        \request()->validate([
            'advert_id'
        ]);
        $advert = Advert::findOrFail(request('advert_id'));
        $exist = Favorite::where([['advert_id', $advert->id], ['user_id', Auth::user()->id]])->first();
        if (is_null($exist)) {
            Favorite::create([
                'user_id' => Auth::user()->id,
                'advert_id' => $advert->id
            ]);
        }
        return response()->json(['statusCode' => 200], 200);
    }

    public function delete()
    {
        \request()->validate([
            'advert_id'
        ]);
        $favorite = Favorite::where([
            ['advert_id', \request('advert_id')], ['user_id', Auth::user()->id]
        ])->firstOrFail();
        $favorite->delete();
        return response()->json(['statusCode' => 200], 200);
    }
}
