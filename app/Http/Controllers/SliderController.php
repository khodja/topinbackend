<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;

class SliderController extends Controller
{
    public function get()
    {
        $data = Slider::all();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }
    public function index()
    {
        $data = Slider::all();
        return view('backend.slider.index', compact('data'));
    }

    public function create()
    {
        return view('backend.slider.create');
    }

    public function edit($id)
    {
        $data = Slider::findOrFail($id);
        $directory = "uploads/slider/".$id;
        $images = \File::glob($directory."/*.jpg");
        return view('backend.slider.edit', compact('data', 'images'));
    }

    public function store(Request $request)
    {
        $slider = Slider::create($request->except('method','_token','image'));
        $this->imageHandle($request, $slider);
        return redirect()->action('SliderController@index')
            ->with('success', 'Успешно добавлено');
    }
    public function update($id,Request $request)
    {
        $slider = Slider::findOrFail($id);
        $this->imageHandle($request, $slider);
        $slider->update($request->except('method','_token','image'));
        return redirect()->action('SliderController@index')
            ->with('success', 'Успешно');
    }

    public function imageHandle($request, $slider)
    {
        if ($request->file('image')) {
            $this->createFolder('slider', $slider->id);
            $images = $request->file('image');
            $this->storeImages($images, $slider->id);
        }
    }

    public function createFolder($directory, $vendorId)
    {
        $directory_path = public_path('uploads/'.$directory);
        $path = public_path('uploads/'.$directory.'/'.$vendorId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($image, $id)
    {
        $filename = strtotime("now").'.jpg';
        $this->removeImages($id);
        $path = public_path('uploads/slider/'.$id.'/'.$filename);
        Image::make($image->getRealPath())->encode('jpg', 60)
            ->fit(1920, 230)
            ->save($path);
        return 1;
    }

    public function removeImages($id)
    {
        $pathToDestroy = public_path('uploads/slider/'.$id.'');
        \File::cleanDirectory($pathToDestroy);
    }
}
