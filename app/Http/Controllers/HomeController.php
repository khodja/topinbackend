<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        \Artisan::call('optimize');
        return view('backend.index');
    }
    public function refresh()
    {
        \Artisan::call('optimize');
        return redirect()->back();
    }
}
