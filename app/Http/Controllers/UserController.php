<?php

namespace App\Http\Controllers;

use App\BalanceHistory;
use App\Region;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    private $simple_user = [
        'name' => 'required',
        'birthday' => 'required',
        'email' => 'required',
        'sex' => 'required',
        'country' => 'required',
    ];
    private $business_user = [
        'email' => 'required',
        'work_time' => 'required',
        'name' => 'required',
        'inn' => 'required',
        'description' => 'required',
        'address' => 'required',
        'website' => 'required',
        'category_id' => 'required',
    ];

    public function getById($id)
    {
        $user = User::find($id);
        return response()->json(['statusCode' => 200, 'user' => $user], 200);
    }

    public function index()
    {
        $data = User::all();
        return view('backend.user.index', compact('data'));
    }

    public function create()
    {
        $regions = Region::whereNull('parent_id')->get();
        return view('backend.user.create', compact('regions'));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('backend.user.edit', compact('data'));
    }

    public function store(Request $request)
    {
        if ($request->is_business === '0') {
            $request->validate($this->simple_user);
        }
        $request['password'] = rand(1000, 9999);
        $user = User::create($request->except('_token'));
        $this->imageHandle($request, $user);
        return redirect()->action('PromoController@index')
            ->with('success', 'Успешно добавлено');
    }

    public function update(Request $request, $id)
    {
        if ($request->is_business == 0) {
            $request->validate($this->simple_user);
        }
        $user = User::findOrFail($id);
        $user->update($request->except('_token', '_method'));
        $this->imageHandle($request, $user);
        return redirect()->action('PromoController@index')
            ->with('success', 'Успешно');
    }

    public function imageHandle($request, $user)
    {
        $images = $request->file('image');
        $banner = $request->file('banner');
        if ($request->hasFile('image')) {
            $this->createFolder('user', $user->id);
            $this->storeImages($images, $user->id, 'user');
        }
        if ($request->hasFile('banner')) {
            $this->createFolder('banner', $user->id);
            $this->storeImages($banner, $user->id, 'banner');
        }
    }

    public function createFolder($directory, $vendorId)
    {
        $directory_path = public_path('uploads/' . $directory);
        $path = public_path('uploads/' . $directory . '/' . $vendorId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($image, $id, $path)
    {
        $filename = strtotime("now") . '.jpg';
        $this->removeImages($id, $path);
        $path = public_path('uploads/' . $path . '/' . $id . '/' . $filename);
        Image::make($image->getRealPath())->encode('jpg', 60)
            ->save($path);
        return 1;
    }

    public function changeToBusiness()
    {
        $user = Auth::user();
        $get_balance = $user->balance;
        $balance = $get_balance->amount * 1;
        if ($balance <= 30000) {
            return response()->json(['statusCode' => 404], 404);
        }
        $get_balance->subtract(30000);
        $limit = $user->limit * 1;
        $change_limit = $limit == 30000 ? 30000 : $limit + 30;
        $is_success = $user->update([
            'is_paid' => true,
            'is_business' => true,
            'limit' => $change_limit
        ]);
        if ($is_success) {
            BalanceHistory::create([
                'user_id' => $user->id,
                'balance_id' => $get_balance->id,
                'amount' => -30000,
            ]);
        }

        return response()->json(['statusCode' => 200], 200);
    }

    public function register(Request $request)
    {
        if (1 * $request['is_business'] === 0) {
            $request->validate($this->simple_user);
        } else {
//            $request->validate($this->business_user);
        }
        $request['birthday'] = Carbon::parse(request('birthday'))->format('Y-m-d');
        $request['sex'] = $request['sex'] * 1 ? 'male' : 'female';
        $user = Auth::user();
        $user->update($request->except('image', 'banner', '_method'));
        $this->imageHandle($request, $user);
        $data = User::where('id', $user->id)->with('balance')->firstOrFail();
        return response()->json(['statusCode' => 200, 'data' => $data], 200);
    }

    public function removeImages($id, $path)
    {
        $pathToDestroy = public_path('uploads/' . $path . '/' . $id . '');
        \File::cleanDirectory($pathToDestroy);
    }
}
