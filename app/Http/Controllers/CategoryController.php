<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\AdvertController;

class CategoryController extends Controller
{
    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_kr' => 'required',
        'description_uz' => 'required',
        'description_ru' => 'required',
        'description_kr' => 'required',
        'color' => 'required',
    ];

    public function index()
    {
        $data = Category::whereNull('parent_id')->get();
        return view('backend.category.index', compact('data'));
    }

    public function show($id)
    {
        $data = Category::where('parent_id', $id)->with('children', 'parent')->get();
        $parent = Category::findOrFail($id);
        return view('backend.category.index', compact('data', 'parent'));
    }

    public function create()
    {
        $data = Category::all();
        return view('backend.category.create', compact('data'));
    }

    public function edit($id)
    {
        $data = Category::findOrFail($id);
        $categories = Category::where('id', '!=', $id)->get();
        $image = $data->image;
        return view('backend.category.edit', compact('data', 'categories', 'image'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }

        $category = Category::create($request->except('image', '_token'));
        $this->imageHandle($request, $category);
        if ($category->parent_id !== null) {
            return redirect()->action('CategoryController@show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->action('CategoryController@index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function update(Request $request, $alias)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }
        $category = Category::where('alias', $alias)->firstOrFail();
        $category->update($request->except('image', '_token',
            '_method'));
        $this->imageHandle($request, $category);
        if ($category->parent_id !== null) {
            return redirect()->action('CategoryController@show', $category->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->action('CategoryController@index')
                ->with('success', 'Успешно добавлено');
        }
    }

    public function imageHandle($request, $category)
    {
        if ($request->file('image')) {
            $this->createFolder('categories', $category->id);
            $images = $request->file('image');
            $this->storeImages($images, $category->id);
        }
    }

    public function createFolder($directory, $vendorId)
    {
        $directory_path = public_path('uploads/'.$directory);
        $path = public_path('uploads/'.$directory.'/'.$vendorId);
        if (!\File::isDirectory($directory_path)) {
            \File::makeDirectory($directory_path, 0777, true, true);
        }
        if (!\File::isDirectory($path)) {
            \File::makeDirectory($path, 0777, true, true);
        }
        return 1;
    }

    public function storeImages($image, $id)
    {
        $filename = strtotime("now").'.png';
        $this->removeImages($id);
        $path = public_path('uploads/categories/'.$id.'/'.$filename);
        Image::make($image->getRealPath())
            ->fit(240, 340)
            ->save($path);
        return 1;
    }

    public function removeImages($id)
    {
        $pathToDestroy = public_path('uploads/categories/'.$id.'');
        \File::cleanDirectory($pathToDestroy);
    }
    public function delete($id){
        $parent = Category::where('id',$id)->firstOrFail();
        $children = $parent->getAllChilds();
        foreach ($children as $child){
            $this->removeImages($child->id);
            $child->types()->delete();
            $child->advert()->delete();
            $child->delete();
        }
        $parent->delete();
        return redirect()->back()->with('success','Успешно');
    }
    /// api starts
    public function get()
    {
        return Category::all();
    }

    public function search(Category $category)
    {
        $keyword = \request('keyword');
        return $category
            ->where('name_ru', 'like', '%'.$keyword.'%')
            ->orWhere('name_uz', 'like', '%'.$keyword.'%')
            ->orWhere('name_kr', 'like', '%'.$keyword.'%')
            ->orWhere('description_ru', 'like', '%'.$keyword.'%')
            ->orWhere('description_uz', 'like', '%'.$keyword.'%')
            ->orWhere('description_kr', 'like', '%'.$keyword.'%')
            ->get();
    }
}
