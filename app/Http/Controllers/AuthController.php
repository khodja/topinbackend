<?php

namespace App\Http\Controllers;

use App\Balance;
use Illuminate\Http\Request;
use App\Phone;
use App\User;
use App\Role;
use Carbon\Carbon;
use Auth;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'birthday' => 'required',
            'name' => 'required',
            'is_business' => 'required',
            'country' => 'required',
            'sex' => 'required',
        ]);
        $user = User::where('phone', request('phone'))->firstOrFail();
        if ($user->id === auth()->user()->getAuthIdentifier()) {
            $user->update([
                'birthday' => Carbon::parse(request('birthday'))->format('Y-m-d'),
                'name' => $request->name,
                'is_business' => $request->is_business,
                'country' => $request->country,
                'sex' => $request->sex,
            ]);
            return response('ok', 200);
        }
        abort(404);
    }

    public function auth(Request $request)
    {
        $request->validate([
            'phone' => 'required',
            'verify_code' => 'required',
        ]);
        $checkForExist = User::where('phone', request('phone'))->first();
        Phone::where([['phone', request('phone')], ['code', request('verify_code')]])->firstOrFail();
        if (!$checkForExist) {
            $user = User::create([
                'phone' => request('phone'),
                'password' => bcrypt(request('verify_code')),
            ]);
            $role = Role::where('type', 'member')->first();
            $user->roles()->attach($role);
        } else {
            $user = $checkForExist;
        }
        if (!$user->balance) {
            Balance::create([
                'user_id' => $user->id
            ]);
        }
        $token = $user->createToken($user->phone);

        $data = [
            'token' => $token->accessToken,
            'user' => $user->name ? User::where('id', $user->id)->with('balance')->first() : null
        ];
        return response(compact('data'), 200);
    }

    public function login()
    {
        $user = User::where(['phone' => request('phone')])->firstOrFail();
        if (\Hash::check(request('password'), $user->password)) {
            $token = $user->createToken($user->phone);
            if (is_null($user->balance)) {
                Balance::create([
                    'user_id',
                    $user->id
                ]);
            }
        } else {
            $response = "Password missmatch";
            return response($response, 422);
        }
        return response()->json($token, 200);
    }

    public function setToken(Request $request)
    {
        $request->validate([
            'notification_token' => 'required'
        ]);
        $user = Auth::user();
        $user->update([
            'notification_token' => $request->notification_token
        ]);
        return response()->json(['statusCode' => 200], 200);
    }

    public function sendNotification(Request $request)
    {
        $user = User::where('notification_token', $request['notification_token'])->firstOrFail();
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'notification_token' => 'required',
        ]);
        $title = $request['title'];
        $body = $request['body'];
        $client = new Client();

        $client->post('https://exp.host/--/api/v2/push/send', [
            'json' => [
                "to" => $user->notification_token,
                "sound" => 'default',
                "title" => $title,
                "body" => $body,
                "data" => ["data" => 'goes here']
            ]
        ]);
        return response()->json(['statusCode' => 200], 200);
    }

    public function verification(Request $request)
    {
        $request->validate([
            'phone' => 'required'
        ]);
        $password = rand(1000, 9999);
        if ($request['phone'] == '990008991') {
            $password = 1234;
        }

        $success = $this->send_code($request->phone, $password);
        Phone::updateOrCreate(['phone' => $request->phone], [
            'phone' => $request->phone,
            'code' => $password,
        ]);
        if (!$success) {
            return response('sms_not_working', 404);
        }
        return response()->json(['statusCode' => 200], 200);
    }

    public function send_code($phone, $password)
    {
        $credentials = base64_encode('topinguz:9!+;Hm80qw*7');
        $verify_password = $password;
        $client = new Client(
            [
                'headers' => ['Content-Type' => 'application/json', 'Authorization' => 'Basic ' . $credentials]
            ]
        );
        $client->post('https://send.smsxabar.uz/broker-api/send', [
            'json' => [
                "messages" => [
                    [
                        "recipient" => $phone,
                        "message-id" => $phone . strval(time()),

                        "sms" => [
                            "originator" => "3700",
                            "content" => ["text" => "Ваш код подтверждения: " . $verify_password . '. Наберите его в поле ввода.']
                        ]
                    ]
                ]
            ]
        ]);
        return true;
    }

    public function getUser()
    {
        $user = Auth::user();
        return User::where('id', $user->id)->with('balance')->firstOrFail();
    }

    public function logout()
    {
        request()->user()->token()->revoke();
        return response()->json(['statusCode' => 200], 200);
    }
}
