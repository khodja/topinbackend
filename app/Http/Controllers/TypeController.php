<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Type;

class TypeController extends Controller
{
    private $validation = [
        'name_uz' => 'required',
        'name_ru' => 'required',
        'name_kr' => 'required',
    ];

    public function byCategory($id)
    {
        $data = Type::where('category_id', $id)->with('children')->orderByRaw('FIELD(changeable, "0", "2", "1")')->get();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function getChildren($id)
    {
        $data = Type::with('children')->where('parent_id', $id)->get();
        return response()->json(['data' => $data, 'statusCode' => 200], 200);
    }

    public function index()
    {
        $data = Type::whereNull('parent_id')->get();
        return view('backend.type.index', compact('data'));
    }

    public function category($id)
    {
        $category = Category::findOrFail($id);
        $data = Type::whereNull('parent_id')->where('category_id', $id)->get();
        return view('backend.type.index', compact('data', 'category'));
    }

    public function show($id)
    {
        $parent = Type::with('category')->findOrFail($id);
        if ($parent->changeable == 1) {
            return redirect()->action('TypeController@index');
        }
        $data = Type::where('parent_id', $id)->with('children', 'parent')->get();

        return view(('backend.type.show'), compact('data', 'parent'));
    }

    public function create()
    {
        $parent = request('parent_id');
        $category = request('category_id');
        if ($parent) {
            $data = Type::find($parent);
        } else {
            $data = $parent;
        }
        if ($category) {
            $category = Category::findOrFail($category);
        } else {
            $category = null;
        }
        return view('backend.type.create', compact('data', 'category'));
    }

    public function edit($id)
    {
        $data = Type::with('category', 'parent')->findOrFail($id);
        return view('backend.type.edit', compact('data'));
    }

    public function store(Request $request)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        } else {
            $parent_type = Type::findOrFail($request['parent_id']);
            $request['parent_id'] = $parent_type->id;
            $request['changeable'] = $parent_type->changeable == 2 || $parent_type->getParents()->where('changeable', 2)->count() ? 2 : 0;
        }
        $type = Type::create($request->except('_token'));
        if ($type->parent_id !== null) {
            return redirect()->action('TypeController@show', $type->parent_id)->with('success',
                'Успешно добавлено');
        } else {
            return redirect()->action('TypeController@category', $type->category_id)
                ->with('success', 'Успешно добавлено');
        }
    }

    public function changeChildren(Request $request)
    {
        $type = Type::where('id', $request['id'])->first();
        if (!($type->changeable == 2 || $type->getParents()->where('changeable', 2)->count())) {
            return redirect()->back();
        }
        $getChildren = $type->getChildsAttribute();
        foreach ($getChildren as $child) {
            if ($child->changeable != 1) {
                $child->update([
                    'changeable' => 2
                ]);
            }
        }
        return redirect()->back()->with('success', 'Успешно');
    }

    public function update(Request $request, $id)
    {
        $request->validate($this->validation);
        if ($request['parent_id'] == 0) {
            $request['parent_id'] = null;
        }
        $type = Type::findOrFail($id);
        $type->update($request->except('_token',
            '_method', 'category_id'));
        if ($type->parent_id !== null) {
            return redirect()->action('TypeController@show', $type->parent_id)->with('success',
                'Успешно изменен!');
        } else {
            return redirect()->action('TypeController@show', $type->id)
                ->with('success', 'Успешно добавлено');
        }
    }

    public function delete($id)
    {
        $parent = Type::where('id', $id)->firstOrFail();
        $children = $parent->getChildsAttribute();
        foreach ($children as $child) {
            $child->advert_type()->delete();
            $child->delete();
        }
        $parent->delete();
        return redirect()->back()->with('success', 'Успешно');
    }
}
