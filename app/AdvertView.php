<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertView extends Model
{
    protected $guarded = [];

    public function advert()
    {
        return $this->belongsTo('App\Advert');
    }

    public static function createViewLog($advert)
    {
        AdvertView::create([
            'advert_id' => $advert->id,
            'slug' => $advert->name,
            'url' => request()->url(),
            'session_id' => \Auth::getSession()->getId(),
            'user_id' => (auth()->check()) ? auth()->id() : null,
            'ip' => request()->ip(),
            'agent' => request()->header('User-Agent')
        ]);
    }
}
