<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $guarded = [];

//    protected $appends = ['childs'];
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function getChildsAttribute()
    {
        $breadcrumb = collect();
        $children = $this->children;

        while ($children->count()) {
            $child = $children->shift();
            $breadcrumb->push($child);
            $children = $children->merge($child->children);
        }
        return $breadcrumb;
    }

    public function getParents()
    {
        $parents = collect([]);

        $parent = $this->parent;

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }

        return $parents->reverse();
    }

    public function advert_type()
    {
        return $this->belongsToMany('App\Advert', 'advert_types')->using('App\AdvertType')
            ->withPivot('value');
//        return $this->belongsToMany('App\Advert', 'advert_types', 'type_id', 'advert_id')->withPivot('value');
    }
}
