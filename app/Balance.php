<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $guarded = [];

    public function history()
    {
        return $this->hasMany('App\BalanceHistory');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function add($value)
    {
        return $this->increment('amount', $value);
    }

    public function subtract($value)
    {
        return $this->decrement('amount', $value);
    }
}
