<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalanceHistory extends Model
{
    protected $guarded = [];

    public function balance()
    {
        return $this->belongsTo('App\Balance');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
