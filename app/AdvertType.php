<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AdvertType extends Pivot
{
    protected $table = 'advert_types';
    protected $guarded = [];

    public function type(){
        return $this->belongsTo('App\Type','value');
    }
    public function advert(){
        return $this->belongsTo('App\Advert','advert_id');
    }
}
