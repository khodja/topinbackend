<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Category extends Model
{
    use HasSlug;

    protected $guarded = [];
    protected $appends = ['image'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name_ru')
            ->saveSlugsTo('alias');
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'alias';
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function adverts()
    {
        return $this->hasMany('App\Advert');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    public function types()
    {
        return $this->hasMany('App\Type');
    }

    public function getImageAttribute()
    {
        $directory = "uploads/categories/".$this->id;
        $images = \File::glob($directory."/*");
        if (count($images) > 0) {
            return asset($images[0]);
        }
        return asset('img/category.png');
    }
//    public function getCountAttribute()
//    {
//        return Advert::whereIn('category_id', $this->getAllChilds())->count();
//    }

    public function getAllChilds()
    {
        $breadcrumb = collect();
        $children = $this->children;

        while ($children->count()) {
            $child = $children->shift();
            $breadcrumb->push($child);
            $children = $children->merge($child->children);
        }
        return $breadcrumb;
    }
}
