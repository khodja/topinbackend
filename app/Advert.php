<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advert extends Model
{

    protected $guarded = [];
    protected $appends = ['image', 'breadcrumb', 'is_premium', 'is_favorite'];

    public function views()
    {
        return $this->hasMany('App\AdvertView');
    }

    public function favorite()
    {
        return $this->hasMany('App\Favorite');
    }

    public function isViewed()
    {
        if (auth()->id() == null) {
            return $this->views()
                ->where('ip', '=', request()->ip())->exists();
        }

        return $this->views()
            ->where(function ($query) {
                $query
                    ->where('session_id', '=', request()->getSession()->getId())
                    ->orWhere('user_id', '=', (auth()->check()));
            })->exists();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function advert_type()
    {
        return $this->belongsToMany('App\Type', 'advert_types')->using('App\AdvertType')
            ->withPivot('value');
    }

    public function getImageAttribute()
    {
        $directory = "uploads/advert/" . $this->id;
        $images = \File::glob($directory . "/*");
        if (count($images) > 0) {
            $assetPath = [];
            foreach ($images as $image) {
                array_push($assetPath, asset($image));
            }
            return $assetPath;
        }
        return [asset('img/no-advert.png')];
    }

    public function getIsPremiumAttribute()
    {
        return $this->promoted_count * 1 > 0;
    }

    public function getIsFavoriteAttribute()
    {
        $user = auth('api')->user();
        if ($user) {
            $is_favorite = Favorite::where([['advert_id', $this->id], ['user_id', $user->id]])->first();
            if ($is_favorite) {
                return true;
            }
            return false;
        }
        return false;
    }

    public function getBreadcrumbAttribute()
    {
        $parents = collect([]);
        $parent = Category::find($this->category_id);

        while (!is_null($parent)) {
            $parents->push($parent);
            $parent = $parent->parent;
        }
        return array_reverse($parents->toArray());
    }
}
