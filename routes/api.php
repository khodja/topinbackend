<?php

Route::get('search', 'AdvertController@search');
Route::get('slider', 'SliderController@get');
Route::get('regions', 'RegionController@get');
Route::get('main', 'AdvertController@get');
Route::get('advert/{id}', 'AdvertController@show');
Route::get('category/search', 'CategoryController@search');
Route::get('category', 'CategoryController@get');
Route::get('category/{id}/types', 'TypeController@byCategory');
Route::get('types/children/{id}', 'TypeController@getChildren');
Route::get('user/by/{id}', 'UserController@getById');

Route::post('payment', 'BalanceHistoryController@handlePayment');

Route::post('verification', 'AuthController@verification');
Route::post('auth', 'AuthController@auth');
Route::post('send/notification', 'AuthController@sendNotification');

Route::group(['middleware' => ['auth:api']], function () {
    Route::get('user', 'AuthController@getUser');
    Route::put('account', 'UserController@register');
    Route::put('business/limit', 'UserController@changeToBusiness');
    Route::put('set/token', 'AuthController@setToken');
    Route::get('messages', 'MessageController@list');
    Route::get('messages/advert/{id}', 'MessageController@show');
    Route::post('send/message', 'MessageController@send');
    Route::get('my/adverts', 'AdvertController@own');
    Route::get('promos', 'PromoController@get');
    Route::put('promote/advert/{id}', 'AdvertController@promote');
    Route::get('my/adverts/{id}', 'AdvertController@showOwn');
    Route::get('switch/status/{id}', 'AdvertController@switchStatus');
    Route::post('add/advert', 'AdvertController@addAdvert');
    Route::put('update/advert/{id}', 'AdvertController@updateApi');
    Route::get('remove/image/advert/{id}', 'AdvertController@removeImage');
    Route::get('favorite', 'FavoriteController@get');
    Route::patch('favorite/set', 'FavoriteController@set');
    Route::patch('favorite/remove', 'FavoriteController@delete');
    Route::get('balance', 'BalanceController@get');
    Route::get('balance/history', 'BalanceHistoryController@get');
    Route::get('payment/uzcard', 'BalanceHistoryController@uzcard');
    Route::get('payment/octo', 'BalanceHistoryController@octo');
    Route::get('logout', 'AuthController@logout');
});
