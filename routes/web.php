<?php

// Route::get('/', function () {
//     return redirect()->action('HomeController@index');
// });

Auth::routes(['register' => false]);


Route::group(['middleware' => ['role:admin']], function () {
    Route::get('dashboard', 'HomeController@index')->name('home');
    Route::get('dashboard/refresh', 'HomeController@refresh');
    Route::get('dashboard/category', 'CategoryController@index');
    Route::get('dashboard/category/show/{id}', 'CategoryController@show');
    Route::get('dashboard/category/create', 'CategoryController@create');
    Route::get('dashboard/category/edit/{id}', 'CategoryController@edit');
    Route::post('dashboard/category/store', 'CategoryController@store');
    Route::put('dashboard/category/update/{id}', 'CategoryController@update');
    Route::delete('dashboard/category/delete/{id}', 'CategoryController@delete');

    Route::get('dashboard/promo', 'PromoController@index');
    Route::get('dashboard/promo/show/{id}', 'PromoController@show');
    Route::get('dashboard/promo/create', 'PromoController@create');
    Route::get('dashboard/promo/edit/{id}', 'PromoController@edit');
    Route::post('dashboard/promo/store', 'PromoController@store');
    Route::put('dashboard/promo/update/{id}', 'PromoController@update');

    Route::get('dashboard/user', 'UserController@index');
    Route::get('dashboard/user/show/{id}', 'UserController@show');
    Route::get('dashboard/user/create', 'UserController@create');
    Route::get('dashboard/user/edit/{id}', 'UserController@edit');
    Route::post('dashboard/user/store', 'UserController@store');
    Route::put('dashboard/user/update/{id}', 'UserController@update');


    Route::get('dashboard/region', 'RegionController@index');
    Route::get('dashboard/region/show/{id}', 'RegionController@show');
    Route::get('dashboard/region/create', 'RegionController@create');
    Route::get('dashboard/region/edit/{id}', 'RegionController@edit');
    Route::post('dashboard/region/store', 'RegionController@store');
    Route::get('dashboard/region/toggle/{id}', 'RegionController@toggleMain');
    Route::put('dashboard/region/update/{id}', 'RegionController@update');


    Route::get('dashboard/slider', 'SliderController@index');
    Route::get('dashboard/slider/create', 'SliderController@create');
    Route::get('dashboard/slider/edit/{id}', 'SliderController@edit');
    Route::post('dashboard/slider/store', 'SliderController@store');
    Route::put('dashboard/slider/update/{id}', 'SliderController@update');

    Route::get('dashboard/category/{id}/type', 'TypeController@category');
    Route::get('dashboard/type', 'TypeController@index');
    Route::get('dashboard/type/show/{id}', 'TypeController@show');
    Route::get('dashboard/type/change/generator/{id}', 'TypeController@changeChildren');
    Route::get('dashboard/type/create', 'TypeController@create');
    Route::get('dashboard/type/edit/{id}', 'TypeController@edit');
    Route::post('dashboard/type/store', 'TypeController@store');
    Route::put('dashboard/type/update/{id}', 'TypeController@update');
    Route::delete('dashboard/type/delete/{id}', 'TypeController@delete');

    Route::get('dashboard/advert', 'AdvertController@index');
    Route::get('dashboard/activate/advert/{id}', 'AdvertController@activate');
    Route::get('dashboard/disable/advert/{id}', 'AdvertController@disable');
    Route::get('dashboard/advert/create', 'AdvertController@create');
    Route::get('dashboard/advert/edit/{id}', 'AdvertController@edit');
    Route::post('dashboard/advert/store', 'AdvertController@store');
    Route::put('dashboard/advert/update/{id}', 'AdvertController@update');
    Route::get('dashboard/advert/removeImage/{id}', 'AdvertController@removeImage');
    Route::get('dashboard/show/advert/{id}', 'AdvertController@show');
});
