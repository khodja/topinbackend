<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en"/>
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="msapplication-TileImage" content="/apple-touch-icon-180x180.png">
    <meta name="application-name" content="Topin Management">
    <title>Topin Management</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,300i,400,400i,500,500i,600,600i,700,700i&amp;subset=latin-ext">
    @yield('style')

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="/apple-touch-icon.png"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png"/>
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png"/>
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png"/>
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png"/>
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png"/>
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png"/>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png"/>

    <script src="{{ asset('backend/js/require.min.js')}}"></script>
    <script src="{{ asset('backend/js/vendors/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('backend/plugins/filer/jquery.filer.js') }}"></script>
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/jquery.filer-dragdropbox-theme.css')}}">
    <script>
        requirejs.config({
            baseUrl: '/'
        });
    </script>
    <link href="{{ asset('backend/css/dashboard.css')}}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/bootstrap-timepicker.min.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/js/dashboard.js')}}"></script>
    <link href="{{ asset('backend/plugins/charts-c3/plugin.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/plugins/charts-c3/plugin.js')}}"></script>
    <link href="{{ asset('backend/plugins/maps-google/plugin.css')}}" rel="stylesheet"/>
    <script src="{{ asset('backend/plugins/maps-google/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/input-mask/plugin.js')}}"></script>
    <script src="{{ asset('backend/plugins/datatables/plugin.js') }}"></script>
    <script src="{{ asset('backend/js/custom.js')}}"></script>
    <script src="{{ asset('backend/js/main.js')}}"></script>
    <link href="{{ asset('backend/css/custom.css')}}" rel="stylesheet"/>
    <link href="{{ asset('backend/css/style.css')}}" rel="stylesheet"/>
</head>
<body>
<div class="page">
    <div class="page-main">
        <header class="header px-4">
            <div class="container-xl">
                <div class="d-flex">
                    <a class="header-brand" href="{{ action('HomeController@index')  }}">
                        <img src="{{ asset('/img/logo.svg') }}" class="h-6" alt="logo">
                    </a>
                    <div class="d-flex order-lg-2 ml-auto">
                        @if(Auth::user()->hasRole('admin'))
                        @endif
                        <div class="dropdown">
                            <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                                <span class="avatar" style="    "></span>
                                <span class="ml-2 d-none d-lg-block">
                        <span class="text-default">{{ Auth::user()->name }}</span>
                    </span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                <form method="POST" action="{{ route('logout') }}">
                                    @csrf
                                    <button class="dropdown-item" type="submit"><i
                                            class="dropdown-icon fe fe-log-out"></i> Выйти
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
                       data-target="#headerMenuCollapse">
                        <span class="header-toggler-icon"></span>
                    </a>
                </div>
            </div>
        </header>
        <div class="header collapse d-lg-flex px-4 py-0" id="headerMenuCollapse">
            <div class="container-xl">
                <div class="row align-items-center">
                    <div class="col-lg order-lg-first">
                        <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                            <li class="nav-item">
                                <a href="{{ action('HomeController@index') }}"
                                   class="nav-link {{ request()->is('dashboard') ? 'active' : '' }}">
                                    <i class="fe fe-activity"></i>
                                    Статистика</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('CategoryController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/category*') ? 'active' : '' }}">
                                    <i class="fe fe-layers"></i>
                                    Категории</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('RegionController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/region*') ? 'active' : '' }}">
                                    <i class="fe fe-map"></i>
                                    Регионы</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('AdvertController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/advert*') ? 'active' : '' }}">
                                    <i class="fe fe-database"></i>
                                    Обьявления</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('SliderController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/slider*') ? 'active' : '' }}">
                                    <i class="fe fe-target"></i>
                                    Слайдер</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('PromoController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/promo*') ? 'active' : '' }}">
                                    <i class="fe fe-layers"></i>
                                    Предложения</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ action('UserController@index') }}"
                                   class="nav-link {{ request()->is('dashboard/user*') ? 'active' : '' }}">
                                    <i class="fe fe-users"></i>
                                    Пользователи</a>
                            </li>
                            @if(Auth::user()->hasRole('admin'))
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @include('partials.message')
        @yield('content')
    </div>
    <footer class="footer">
        <div class="container">
            <div class="row align-items-center flex-row-reverse">
                <div class="col-12 mt-3 mt-lg-0 text-center">
                    Copyright © 2020. Все права защищены.
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="{{asset('js/vue.js')}}"></script>
@yield('script')
</body>
</html>
