@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Предложения
                    <span class="badge badge-dark">{{ isset($parent) ? $parent->name_ru : ''}}</span>
                </h3>
                <a href="{{action('PromoController@create')}}" class="btn btn-outline-success">Добавить</a>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Кол-во премиум показов</th>
                        <th>Цена</th>
{{--                        <th></th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->name_ru }}</td>
                            <td>{{ $datas->count }}</td>
                            <td>{{ number_format($datas->price) }} UZS</td>
                            {{--            <td>--}}
                            {{--            <form action="{{ action('PromoController@delete' , $datas->id) }}" method="POST">--}}
                            {{--            @method('DELETE')--}}
                            {{--            @csrf--}}
                            {{--                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">--}}
                            {{--                    <i class="fe fe-trash"></i>--}}
                            {{--                </button>--}}
                            {{--            </form>--}}
                            {{--            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
