@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('PromoController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-ru">Название Ru</label>
                                    <input required="required" name="name_ru" type="text" class="form-control"
                                           id="name-ru" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-uz">Название Uz</label>
                                    <input required="required" name="name_uz" type="text" class="form-control"
                                           id="name-uz" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-kr">Название KR</label>
                                    <input required="required" name="description_kr" type="text" class="form-control"
                                           id="name-kr" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-ru">Описание Ru</label>
                                    <input required="required" name="description_ru" type="text" class="form-control"
                                           id="description-ru" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-uz">Описание Uz</label>
                                    <input required="required" name="description_uz" type="text" class="form-control"
                                           id="description-uz" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-kr">Описание KR</label>
                                    <input required="required" name="name_kr" type="text" class="form-control"
                                           id="description-kr" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена</label>
                                    <input required="required" name="price" type="number" class="form-control"
                                           id="price" placeholder="Введите цену"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="count">Кол-во премиум показов</label>
                                    <input required="required" name="count" type="number" class="form-control"
                                           id="price" placeholder="Введите Кол-во"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

