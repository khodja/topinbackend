@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('RegionController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_kr">Название (KR)</label>
                                    <input required="required" type="text" name="name_kr" value="{{ $data->name_kr }}"
                                           class="form-control regStepOne" id="name_kr" placeholder=""/>
                                </div>
                            </div>
                            @if($data->parent_id != null)
                                <div class="input-block">
                                    <div class="input">
                                        <label>Привязанный регион:</label>
                                        <select name="parent_id" class="form-control custom-select">
                                            <option value="0">Отвязать от всех</option>
                                            @foreach( $categories as $datas )
                                                <option value="{{ $datas->id }}"
                                                        @if($data->parent_id == $datas->id) selected @endif>{{ $datas->name_ru }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="input-block pb-3 jv_map_register">
                                <div class="input">
                                    <label>Локация:</label>
                                    <div id="map" style="height: 400px; width: 100%;"></div>
                                    <input id="long" value="{{$data->long}}" name="long" type="hidden"
                                           class="form-control">
                                    <input id="lat" value="{{$data->lat}}" name="lat" type="hidden"
                                           class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    {{--    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>--}}
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
    <script src="{{asset('backend/js/vendors/jscolor.js')}}"></script>
    <script>
        var lattitude = $('#lat');
        var longitude = $('#long');

        function initMap() {
            var latlng = new google.maps.LatLng({{$data->lat}}, {{$data->long}});
            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 12,
                animation: google.maps.Animation.BOUNCE
            });
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true
            });
            var lat, long;
            google.maps.event.addListener(marker, 'dragend', function (event) {
                lat = this.getPosition().lat().toFixed(6);
                long = this.getPosition().lng().toFixed(6);
                lattitude.val(lat);
                longitude.val(long);
            });
        }
    </script>
@endsection
