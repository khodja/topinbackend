@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Регион
                    <span class="badge badge-dark">{{ isset($parent) ? $parent->name_ru : ''}}</span>
                </h3>
                <form action="{{ action('RegionController@create') }}">
                    @isset($parent)
                    <input type="hidden" name="parent_id" value="{{$parent->id}}">
                    @endif
                    <button class="btn btn-outline-success">Добавить</button>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->name_ru }}</td>
                            <td class="text-right">
                                <a href="{{ action('RegionController@edit' , $datas->id) }}"
                                   class="btn btn-outline-dark">Изменить</a>
                                <a href="{{ action('RegionController@show' , $datas->id) }}"
                                   class="btn btn-primary"><i class="fe fe-arrow-right"></i></a>
                            </td>
                            {{--            <td>--}}
                            {{--            <form action="{{ action('RegionController@delete' , $datas->id) }}" method="POST">--}}
                            {{--            @method('DELETE')--}}
                            {{--            @csrf--}}
                            {{--                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">--}}
                            {{--                    <i class="fe fe-trash"></i>--}}
                            {{--                </button>--}}
                            {{--            </form>--}}
                            {{--            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
