@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('RegionController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить Регион</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-ru">Название Ru</label>
                                    <input required="required" name="name_ru" type="text" class="form-control"
                                           id="name-ru" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-uz">Название Uz</label>
                                    <input required="required" name="name_uz" type="text" class="form-control"
                                           id="name-uz" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-kr">Название KR</label>
                                    <input required="required" name="name_kr" type="text" class="form-control"
                                           id="name-kr" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="custom_id">Привязанный регион:</label>
                                    <select id="custom_id" name="parent_id" class="form-control custom-select">
                                        <option value="0">Нет привязанности</option>
                                        @foreach( $data as $datas )
                                            <option
                                                value="{{ $datas->id }}" {{ request()->has('parent_id') ? (request()->get('parent_id') == $datas->id ? 'selected' : '') : '' }}>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block pb-3 jv_map_register">
                                <div class="input">
                                    <label>Локация:</label>
                                    <div id="map" style="height: 400px; width: 100%;"></div>
                                    <input id="long" value="69.240562" name="long" type="hidden" class="form-control">
                                    <input id="lat" value="41.311081" name="lat" type="hidden" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
{{--    <script async defer--}}
{{--            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs3cvxdAATTvzZ-srgPAID1d2IZHuZcZE&callback=initMap"></script>--}}
    <script async defer src="https://maps.googleapis.com/maps/api/js?callback=initMap"></script>
    <script>
        let latitude = $('#lat');
        let longitude = $('#long');

        function initMap() {
            let latlng = new google.maps.LatLng(41.311081, 69.240562);
            let map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 12,
                animation: google.maps.Animation.BOUNCE
            });
            let marker = new google.maps.Marker({
                position: latlng,
                map: map,
                draggable: true
            });
            let lat, long;
            google.maps.event.addListener(marker, 'dragend', function (event) {
                lat = this.getPosition().lat().toFixed(6);
                long = this.getPosition().lng().toFixed(6);
                // console.log(lat + 'lat' + long + 'lng')
                latitude.val(lat);
                longitude.val(long);
            });
        }
    </script>
@endsection
