@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('TypeController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (RU)</label>
                                    <input required="required" type="text" name="name_ru" value="{{ $data->name_ru }}"
                                           class="form-control regStepOne" id="name_ru" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (UZ)</label>
                                    <input required="required" type="text" name="name_uz" value="{{ $data->name_uz }}"
                                           class="form-control regStepOne" id="name_uz" placeholder=""/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_kr">Название (KR)</label>
                                    <input required="required" type="text" name="name_kr" value="{{ $data->name_kr }}"
                                           class="form-control regStepOne" id="name_kr" placeholder=""/>
                                </div>
                            </div>
                            @if($data->parent)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="custom_id">Привязанная Тип:</label>
                                        <select id="custom_id" name="parent_id" class="form-control custom-select">
                                                <option
                                                    value="{{ $data->parent->id }}"
                                                    >{{ $data->parent->name_ru }}</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if($data->category_id)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="category_id">Выберите категорию</label>
                                        <select id="category_id" disabled name="category_id"
                                                class="form-control custom-select">
                                            <option
                                                value="{{ $data->category_id }}">{{ $data->name_ru }}</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="input-block">
                                <div class="input">
                                    <label for="changeable">Тип ввода</label>
                                    <select id="changeable" disabled name="changeable"
                                            class="form-control custom-select">
                                        <option value="0" @if($data->changeable == 0) selected @endif>Выбор</option>
                                        <option value="1" @if($data->changeable == 1) selected @endif>Ввод текста</option>
                                        <option value="2" @if($data->changeable == 2) selected @endif>Генератор выбора</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
@endsection
