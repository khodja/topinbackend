@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Тип обьявления</h3>
                <div>
                    @isset($category)
                        Категория :
                        <a href="{{action('TypeController@category',$category->id)}}"
                           class="badge badge-success">{{  $category->name_ru }}</a>
                    @endisset
                    @isset($parent)
                        @if($parent->category)
                            Категория :
                            <a href="{{action('TypeController@category',$parent->category->id)}}"
                               class="badge badge-success">{{  $parent->category->name_ru }}</a>
                        @endif
                        @if(count($data))
                            Привязанный тип :
                            @foreach($data[0]->getParents() as $datas)
                                <a href="{{action('TypeController@show',$datas->id)}}"
                                   class="badge badge-warning">{{  $datas->name_ru }}</a>
                            @endforeach
                        @else
                            <a href="{{action('TypeController@show',$parent->id)}}"
                               class="badge badge-warning">{{  $parent->name_ru }}</a>
                        @endif
                    @endisset
                </div>
                <form action="{{ action('TypeController@create') }}">
                    {{--                    @isset($category)--}}
                    {{--                        <input type="hidden" name="category_id" value="{{$category->id}}">--}}
                    {{--                    @endif--}}
                    @isset($category)
                        <input type="hidden" name="category_id" value="{{$category->id}}">
                    @endif
                    @isset($parent)
                        <input type="hidden" name="parent_id" value="{{$parent->id}}">
                    @endif
                    <button class="btn btn-outline-success">Добавить</button>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->name_ru }}</td>
                            <td class="text-right">
                                <a href="{{ action('TypeController@edit' , $datas->id) }}"
                                   class="btn btn-dark">Изменить</a>
                                @if($datas->changeable * 1 === 2 && $datas->getParents()->count() < 5)
                                    <a href="{{ action('TypeController@show' , $datas->id) }}"
                                       class="btn btn-primary">Связки <i class="fe fe-arrow-right"></i></a>
{{--                                    <a href="{{ action('TypeController@changeChildren' , $datas->id) }}"--}}
{{--                                       class="btn btn-primary">Изменить связки генратора <i class="fe fe-arrow-right"></i></a>--}}
                                @endif
                                @if($datas->changeable * 1 === 0 && ($datas->getParents()->where('changeable',2)->count()))
                                    <a href="{{ action('TypeController@show' , $datas->id) }}"
                                       class="btn btn-primary">Связки <i class="fe fe-arrow-right"></i></a>
                                @endif
                                <form action="{{ action('TypeController@delete' , $datas->id) }}" class="d-inline-block" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger" onclick="return confirm('Вы уверены?')">
                                        <i class="fe fe-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
