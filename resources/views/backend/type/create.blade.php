@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('TypeController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_ru">Название (Ru)</label>
                                    <input required="required" name="name_ru" type="text" class="form-control"
                                           id="name_ru" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_uz">Название (Uz)</label>
                                    <input required="required" name="name_uz" type="text" class="form-control"
                                           id="name_uz" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name_kr">Название (KR)</label>
                                    <input required="required" name="name_kr" type="text" class="form-control"
                                           id="name_kr" placeholder="Введите название"/>
                                </div>
                            </div>
                            @if($data)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="parent_id">Привязанная Тип:</label>
                                        <select id="parent_id" name="parent_id" class="form-control custom-select">
                                            <option
                                                value="{{ $data->id }}">{{ $data->name_ru }}</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if($category)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="category_id">Привязанная категория</label>
                                        <select id="category_id" name="category_id" class="form-control custom-select">
                                            <option
                                                value="{{ $category->id }}">{{ $category->name_ru }}</option>
                                        </select>
                                    </div>
                                </div>
                            @endif
                            @if($data)
                                @if($data->changeable == 2)
                                    <div class="input-block">
                                        <div class="input">
                                            <label for="changeable">Тип ввода</label>
                                            <select id="changeable" name="changeable"
                                                    class="form-control custom-select">
                                                <option value="2">Генератор выбора</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                @if($data->getParents()->where('changeable',2)->count() > 3)
                                    <div class="input-block">
                                        <div class="input">
                                            <label for="changeable">Тип ввода</label>
                                            <select id="changeable" name="changeable"
                                                    class="form-control custom-select">
                                                <option value="0">Выбор</option>
                                                <option value="1">Ввод текста</option>
                                            </select>
                                        </div>
                                    </div>
                                @endif
                            @else
                                @if(!$data)
                                    <div class="input-block">
                                        <div class="input">
                                            <label for="changeable">Тип ввода</label>
                                            <select id="changeable" name="changeable"
                                                    class="form-control custom-select">
                                                <option value="0">Выбор</option>
                                                <option value="1">Ввод текста</option>
                                                <option value="2">Генератор выбора</option>
                                            </select>
                                        </div>
                                    </div>
                                @else
                                    @if($data->getParents()->where('changeable',2)->count())
                                        <div class="input-block">
                                            <div class="input">
                                                <label for="changeable">Тип ввода</label>
                                                <select id="changeable" name="changeable"
                                                        class="form-control custom-select">
                                                    <option value="0">Выбор</option>
                                                    <option value="1">Ввод текста</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
