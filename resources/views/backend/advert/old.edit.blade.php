@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('AdvertController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head"><h3>Изменить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" name="name" value="{{$data->name}}" type="text"
                                           class="form-control"
                                           id="name" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description">Описание</label>
                                    <input required="required" name="description" value="{{$data->description}}" type="text" class="form-control"
                                           id="description" placeholder="Введите Описание"/>
                                </div>
                            </div>
                            <input type="hidden" name="category_id" value="{{$category->id}}">
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена</label>
                                    <input required="required" name="price" value="{{$data->price}}" type="number"
                                           class="form-control"
                                           id="price" placeholder="Введите цену"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="region_id">Регион:</label>
                                    <select id="region_id" name="region_id" class="form-control custom-select">
                                        @foreach( $regions as $region )
                                            <option
                                                value="{{ $region->id }}" {{ $region->id == $data->region_id ? 'selected' : '' }}>{{ $region->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach($types as $type)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="advert_{{$type->id}}">{{$type->name_ru}}</label>
                                        <input type="hidden" name="type_id[]" value="{{$type->id}}">
                                        @if($type->changeable == 1)
                                            <input required="required" name="advert_type[]" type="number"
                                                   class="form-control"
                                                   value="{{$chosen_types->where('id', $type->id)->first() ? $chosen_types->where('id', $type->id)->first()->pivot->value : ''}}"
                                                   id="advert_{{$type->id}}" placeholder="Введите {{$type->name_ru}}"/>
                                        @else
                                            @if($type->children()->count() > 0 )
                                                <select name="advert_type[]" class="form-control custom-select"
                                                        id="advert_{{$type->id}}">
                                                    @foreach($type->children as $key => $child)
                                                        <option value="{{$child->id}}"
                                                                @if($chosen_types->where('id', $type->id)->first())
                                                                @if($chosen_types->where('id', $type->id)->first()->pivot->value == $child->id)
                                                                selected
                                                            @endif
                                                            @endif
                                                        >{{$child->name_ru}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте фотографии.</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($images as $image)
                                        <li class="gallryUploadBlock_item photo-thumbler d-inline-block"
                                            data-image="{{basename($image)}}">
                                            <div class="jFiler-item-thumb-image">
                                                <img src="{{asset($image)}}" alt="image">
                                            </div>
                                            <div class="removeItem">
                                            <span class="deletePhoto" data-image="{{basename($image)}}">
                                                <i class="fe fe-minus"></i>
                                            </span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                                <input type="file" name="images[]" class="filer_input3" multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $(".filer_input3").filer({
                limit: null,
                maxSize: null,
                extensions: null,
                changeInput: '<a class="btnAddImageToGallery galleryAddElement"><i class="fe fe-plus"></i></a>',
                showThumbs: true,
                theme: "dragdropbox",
                templates: {
                    box: '<ul class="jFiler-items-list buttonAdder jFiler-items-grid"></ul>',
                    item: `<li class="gallryUploadBlock_item jFiler-item">
    {{'{{fi-image}'.'}'}}
                    <div class="removeItem"  ><span><i class="fe fe-minus"></i></span></div>
                    </li>`,
                    progressBar: '<div class="bar"></div>',
                    itemAppendToEnd: true,
                    canvasImage: true,
                    removeConfirmation: false,
                    _selectors: {
                        list: '.jFiler-items-list',
                        item: '.jFiler-item',
                        progressBar: '.bar',
                        remove: '.fe.fe-minus'
                    }
                },
                dragDrop: {
                    dragEnter: null,
                    dragLeave: null,
                    drop: null,
                    dragContainer: null,
                },
                files: null,
                addMore: true,
                allowDuplicates: false,
                clipBoardPaste: true,
                excludeName: null,
                beforeRender: null,
                afterRender: null,
                beforeShow: null,
                beforeSelect: null,
                itemAppendToEnd: true,
                onSelect: null,
                afterShow: function (jqEl, htmlEl, parentEl, itemEl) {
                    $('.galleryAddElement').hide()
                    $('.cloneGalleryAddElement').remove()
                    $('.buttonAdder').append('<a class="btnAddImageToGallery cloneGalleryAddElement"><i class="fe fe-plus"></i></a>')
                },
                onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                    let filerKit = inputEl.prop("jFiler"),
                        file_name = filerKit.files_list[id].name;
                    if (filerKit.files_list.length == 1) {
                        $('.galleryAddElement').show()
                    }
                },
                onEmpty: null,
                options: null,
            });
            $('.uploader').on('click', '.cloneGalleryAddElement', function (e) {
                $('.galleryAddElement').trigger('click');
            });
            $('.deletePhoto').click((e) => {
                let removeImage = $(e.target);
                let imageData = removeImage.data('image');
                if (confirm('Вы уверены?')) {
                    let deleteItem = $.get("{{action('AdvertController@removeImage',$data->id)}}", {file_name: imageData});
                    deleteItem.done(() => {
                        removeImage.parent().parent().remove();
                    });
                }
            });
        });
    </script>
@endsection
