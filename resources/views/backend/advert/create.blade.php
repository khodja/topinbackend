@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('AdvertController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" name="name" type="text" class="form-control"
                                           id="name" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description">Описание</label>
                                    <input required="required" name="description" type="text" class="form-control"
                                           id="description" placeholder="Введите Описание"/>
                                </div>
                            </div>
                            <input type="hidden" name="category_id" value="{{$category->id}}">
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена</label>
                                    <input required="required" name="price" type="number" class="form-control"
                                           id="price" placeholder="Введите цену"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="address_info">Информация аддеса</label>
                                    <input required="required" name="address_info" type="text" class="form-control"
                                           id="address_info" placeholder="Введите Информация аддеса"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="region_id">Регион:</label>
                                    <select id="region_id" name="region_id" class="form-control custom-select">
                                        @foreach( $regions as $region )
                                            <option
                                                value="{{ $region->id }}" {{ request()->has('region_id') ? (request()->get('region_id') == $region->id ? 'selected' : '') : '' }}>{{ $region->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div id="create"></div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="jv_file_uploader_text pt-3">Добавьте фотографии.</p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="images[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script>
        let __origDefine = define;
        define = null;
    </script>
    <script src="{{asset('js/vue.js')}}"></script>
    <script>
        let app = new Vue({
            el: '#create',
            data: {
                types: {!! $types !!},
                childTypes: [],
                chosen: [],
            },
            mounted() {
                // console.warn(this.types);
            },
            methods: {
                onSelect: function (event, type) {
                    const value = parseInt(event.target.value);
                    if (value > 0) {
                        this.getChildTypes(value).then(val => {
                            let newData = [{id: type.id, value: value}];
                            const toRemove = this.getAllChildren(type);
                            this.chosen = this.mergeDataById([...newData, ...this.chosen.filter(el => !toRemove.includes( el.id ))])
                        });
                    } else {
                        const toRemove = this.getAllChildren(type);
                        this.chosen = this.chosen.filter(choice => choice.id !== type.id).filter(el => !toRemove.includes( el.id ));
                    }
                },
                getAllChildren: function (data) {
                    let childrenIds = [];
                    let pusher = data.children.map(child => child.id);
                    while (pusher.length) {
                        childrenIds.push(...pusher);
                        const children = []
                        this.childTypes.forEach(child => {
                            pusher.forEach(item => {
                                if(parseInt(item) === parseInt(child.parent_id)) children.push(child.id);
                            })
                        })
                        pusher = children;
                    }
                    return childrenIds;
                },
                getChildTypes: function (id) {
                    return fetch(`http://topin-system.indev.uz/api/types/children/${id}`)
                        .then(response => response.json())
                        .then(value => {
                            this.childTypes = this.mergeDataById([...this.childTypes, ...value.data]);
                        })
                },
                mergeDataById: function (data) {
                    return data.filter(function (a) {
                        if (!this[a.id]) {
                            this[a.id] = true;
                            return true;
                        }
                    }, Object.create(null));
                },
                checkForRelevance: function (inner) {
                    return this.chosen.filter(choice => parseInt(choice.value) === parseInt(inner.parent_id)).length;
                },
            },
            computed: {},
            template: `
                <div>
                    <template v-for="(type, key) in types">
                    <div class="input-block">
                        <div class="input">
                            <label :for="'advert_'+type.id">@{{type.name_ru}}</label>
                            <input type="hidden" name="type_id[]" :value="type.id">
                                    <template v-if="parseInt(type.changeable) === 1">
                                    <input required="required" name="advert_type[]" type="number"
                                       class="form-control"
                                       :id="'advert_'+type.id" :placeholder="'Введите '+ type.name_ru"/>
                                    </template>
                                    <template v-else>
                                        <select @change="onSelect($event , type)" name="advert_type[]" class="form-control custom-select"
                                                :id="'advert_'+type.id">
                                                 <option value="0">Не выбран</option>
                                                <option v-for="child in type.children" :value="child.id">@{{child.name_ru}}</option>
                                        </select>
                                    </template>
                        </div>
                    </div>
                        <div class="input-block" v-for="inner in childTypes" v-if="checkForRelevance(inner) && parseInt(type.changeable) === 2">
                            <div class="input" v-if="inner.children.length">
                                <label :for="'advert_'+inner.id"> @{{inner.name_ru}}</label>
                                <input type="hidden" name="type_id[]" :value="inner.id">
                                        <template v-if="parseInt(inner.changeable) === 1">
                                        <input required="required" name="advert_type[]" type="number"
                                           class="form-control"
                                           :id="'advert_'+inner.id" :placeholder="'Введите '+ inner.name_ru"/>
                                        </template>
                                        <template v-else>
                                            <select @change="onSelect($event , inner)"  name="advert_type[]" class="form-control custom-select"
                                                    :id="'advert_'+inner.id">
                                                     <option value="0">Не выбран</option>
                                                    <option v-for="child in inner.children" :value="child.id">@{{child.name_ru}}</option>
                                            </select>
                                        </template>
                            </div>
                        </div>
                    </template>
                </div>
                `,
        });
    </script>
@endsection
