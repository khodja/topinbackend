@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('AdvertController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name">Название</label>
                                    <input required="required" name="name" type="text" class="form-control"
                                           id="name" placeholder="Введите название"/>
                                </div>
                            </div>
                            <input type="hidden" name="category_id" value="{{$category->id}}">
                            <div class="input-block">
                                <div class="input">
                                    <label for="price">Цена</label>
                                    <input required="required" name="price" type="number" class="form-control"
                                           id="price" placeholder="Введите цену"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="region_id">Регион:</label>
                                    <select id="region_id" name="region_id" class="form-control custom-select">
                                        @foreach( $regions as $region )
                                            <option
                                                value="{{ $region->id }}" {{ request()->has('region_id') ? (request()->get('region_id') == $region->id ? 'selected' : '') : '' }}>{{ $region->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @foreach($types as $type)
                                <div class="input-block">
                                    <div class="input">
                                        <label for="advert_{{$type->id}}">{{$type->name_ru}}</label>
                                        <input type="hidden" name="type_id[]" value="{{$type->id}}">
                                        @if($type->changeable)
                                            <input required="required" name="advert_type[]" type="number"
                                                   class="form-control"
                                                   id="advert_{{$type->id}}" placeholder="Введите {{$type->name_ru}}"/>
                                        @else
                                            @if($type->children()->count() > 0)
                                                <select name="advert_type[]" class="form-control custom-select"
                                                        id="advert_{{$type->id}}">
                                                    @foreach($type->children as $key => $child)
                                                        <option value="{{$child->id}}">{{$child->name_ru}}</option>
                                                    @endforeach
                                                </select>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Общие фотографии</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="jv_file_uploader_text pt-3">Добавьте фотографии.</p>
                            <div class="fotoUploader">
                                <input required="required" type="file" name="images[]" class="filer_input2"
                                       multiple="multiple">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')

@endsection
