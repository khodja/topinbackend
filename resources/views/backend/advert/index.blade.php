@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Все обьявления</h3>
{{--                <form action="{{ action('AdvertController@create') }}">--}}
{{--                    @isset($parent)--}}
{{--                        <input type="hidden" name="parent_id" value="{{$parent->id}}">--}}
{{--                    @endif--}}
{{--                    <button class="btn btn-outline-success">Добавить</button>--}}
{{--                </form>--}}
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Статус</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->name }}</td>
                            @if($datas->status == 'ACTIVE')
                            <td>Активен</td>
                            @endif
                            @if($datas->status == 'MODERATION')
                            <td>Ждет модерации</td>
                            @endif
                            @if($datas->status == 'DISABLED')
                            <td>Отключен</td>
                            @endif
                            <td class="text-right">
                                <a href="{{ action('AdvertController@edit' , $datas->id) }}"
                                   class="btn btn-outline-dark">Модерация</a>
                                <a href="{{ action('AdvertController@activate' , $datas->id) }}"
                                   class="btn btn-success">Активировать</a>
                                <a href="{{ action('AdvertController@disable' , $datas->id) }}"
                                   class="btn btn-danger">Отключить</a>
{{--                                    <a href="{{ action('AdvertController@show' , $datas->id) }}"--}}
{{--                                       class="btn btn-primary"><i class="fe fe-arrow-right"></i></a>--}}
                            </td>
                            {{--            <td>--}}
                            {{--            <form action="{{ action('AdvertController@delete' , $datas->id) }}" method="POST">--}}
                            {{--            @method('DELETE')--}}
                            {{--            @csrf--}}
                            {{--                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">--}}
                            {{--                    <i class="fe fe-trash"></i>--}}
                            {{--                </button>--}}
                            {{--            </form>--}}
                            {{--            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
