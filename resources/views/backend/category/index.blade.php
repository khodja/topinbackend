@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Категории
                    <span class="badge badge-dark">{{ isset($parent) ? $parent->name_ru : ''}}</span>
                </h3>
                <form action="{{ action('CategoryController@create') }}">
                    @isset($parent)
                        <input type="hidden" name="parent_id" value="{{$parent->id}}">
                    @endif
                    <button class="btn btn-outline-success">Добавить</button>
                </form>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Photo</th>
                        <th>Алиас</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->name_ru }}</td>
                            <td><img src="{{ $datas->image }}" style="object-fit: contain;" width="80"></td>
                            <td>{{ $datas->alias }}</td>
                            <td class="text-right">
                                <form action="{{ action('AdvertController@create') }}" class="d-inline-block">
                                    <input type="hidden" name="category_id" value="{{$datas->id}}">
                                    <button class="btn btn-success">Добавить Объявление</button>
                                </form>
                                <a href="{{ action('TypeController@category' , $datas->id) }}"
                                   class="btn btn-info">Типы категории</a>
                                <a href="{{ action('CategoryController@edit' , $datas->id) }}"
                                   class="btn btn-dark">Изменить</a>
                                <a href="{{ action('CategoryController@show' , $datas->id) }}"
                                   class="btn btn-primary">Связанные категории <i class="fe fe-arrow-right"></i></a>
                                @if($datas->parent_id)
                                <form action="{{ action('CategoryController@delete' , $datas->id) }}" class="d-inline-block" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-danger" onclick="return confirm('Вы уверены?')">
                                        <i class="fe fe-trash"></i>
                                    </button>
                                </form>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
