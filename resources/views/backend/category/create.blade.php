@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('CategoryController@store') }}" method="POST"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="white-block mb-30">
                        <div class="head"><h3>Добавить</h3></div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-ru">Название Ru</label>
                                    <input required="required" name="name_ru" type="text" class="form-control"
                                           id="name-ru" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-uz">Название Uz</label>
                                    <input required="required" name="name_uz" type="text" class="form-control"
                                           id="name-uz" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="name-kr">Название KR</label>
                                    <input required="required" name="name_kr" type="text" class="form-control"
                                           id="name-kr" placeholder="Введите название"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-ru">Описание Ru</label>
                                    <input required="required" name="description_ru" type="text" class="form-control"
                                           id="description-ru" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-uz">Описание Uz</label>
                                    <input required="required" name="description_uz" type="text" class="form-control"
                                           id="description-uz" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="description-kr">Описание KR</label>
                                    <input required="required" name="description_kr" type="text" class="form-control"
                                           id="description-kr" placeholder="Введите описание"/>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="custom_id">Привязанная категория:</label>
                                    <select id="custom_id" name="parent_id" class="form-control custom-select">
                                        <option value="0">Нет привязанности</option>
                                        @foreach( $data as $datas )
                                            <option
                                                value="{{ $datas->id }}" {{ request()->has('parent_id') ? (request()->get('parent_id') == $datas->id ? 'selected' : '') : '' }}>{{ $datas->name_ru }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="input-block">
                                <div class="input">
                                    <label for="color">Выберите цвет: </label> <br>
                                    <input required="required" class="jscolor" id="color" name="color"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Фотография</h3>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте фотографию категории. Размер изображений 120x170 пикс.</p>
                            <div class="uploader">
                                <input type="file" name="image">
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('backend/js/vendors/jscolor.js')}}"></script>
@endsection
