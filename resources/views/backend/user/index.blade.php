@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Пользвотели</h3>
                <a href="{{action('UserController@create')}}" class="btn btn-outline-success">Добавить</a>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $key => $datas)
                        @if($key !== 0)
                            <tr>
                                <td>{{ $datas->name }}</td>
                                <td>{{ $datas->phone }}</td>
                                <td>{{ $datas->email }}</td>
                                {{--            <td>--}}
                                {{--            <form action="{{ action('UserController@delete' , $datas->id) }}" method="POST">--}}
                                {{--            @method('DELETE')--}}
                                {{--            @csrf--}}
                                {{--                <button class="btn icon border-0" onclick="return confirm('Вы уверены?')">--}}
                                {{--                    <i class="fe fe-trash"></i>--}}
                                {{--                </button>--}}
                                {{--            </form>--}}
                                {{--            </td>--}}
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
