@extends('layouts.backend')

@section('content')
    <div class="container-xl">
        <div class="card my-3 my-md-5">
            <div class="card-header justify-content-between">
                <h3 class="card-title">Слайдер</h3>
                <a class="btn btn-success" href="{{action('SliderController@create')}}">Добавить</a>
            </div>
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Photo</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $datas)
                        <tr>
                            <td>{{ $datas->id }}</td>
                            <td><img src="{{ $datas->image }}" alt="{{$datas->id}}" width="80"></td>
                            <td class="text-right">
                                <a href="{{ action('SliderController@edit' , $datas->id) }}"
                                   class="btn btn-outline-dark">Изменить</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
