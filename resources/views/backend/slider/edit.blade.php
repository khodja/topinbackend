@extends('layouts.backend')

@section('content')
    <section>
        <div class="container">
            <div class="row">
                <form action="{{ action('SliderController@update',$data->id) }}" method="POST"
                      enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <div class="white-block mb-30">
                        <div class="head">
                            <h3>Изменить</h3>
                        </div>
                        <div class="content">
                            <div class="input-block">
                                <div class="input">
                                    <label for="url">Ссылка</label>
                                    <input name="url" value="{{$data->url}}" type="text" class="form-control"
                                           id="url" placeholder="Введите ссылку"/>
                                </div>
                            </div>
                        </div>
                        <div class="card-header-after">
                            <p class="pt-3">Добавьте фотографию для изменения. Размер изображений 1920x230 пикс</p>
                            <div class="uploader">
                                <ul class="jFiler-items-list jFiler-items-grid">
                                    @foreach($images as $image)
                                        <img src="{{asset($image)}}?t='{{microtime(true)}}" alt="image" width="200">
                                    @endforeach
                                </ul>
                                <input type="file" name="image" required>
                            </div>
                        </div>
                    </div>
                    <div class="button-block">
                        <button type="submit" class="continue-btn">Сохранить</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection

@section('script')
    <script src="{{asset('backend/js/vendors/jscolor.js')}}"></script>
@endsection
